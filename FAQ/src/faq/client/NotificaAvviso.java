package faq.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Classe che gestisce la finestra di dialogo per notifiche all'utente
 */
public class NotificaAvviso extends DialogBox
{
  interface NotificaAvvisoBinder extends UiBinder<Widget, NotificaAvviso> {}
  private static NotificaAvvisoBinder uiBinder = GWT.create(NotificaAvvisoBinder.class);
  
  @UiField Button ok;
  @UiField Label message;
  
  public NotificaAvviso(String type, String message)
  {
    setWidget(uiBinder.createAndBindUi(this));
    if(type.equals("alert"))
    {
      this.setText("Avviso");
    } else if(type.equals("notify"))
    {
      this.setText("Notifica");
    }
    this.message.setText(message);
    setAutoHideEnabled(true);
    this.center();
    this.show();
  }

  //Associo il clickHandler al Button attraverso l'UiHandler
  @UiHandler("ok")
  void handleClick(ClickEvent event)
  {
    this.hide();
  }
}
