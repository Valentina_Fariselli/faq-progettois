package faq.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import faq.shared.User;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica dell'Amministratore per
 * la visualizzazione della finestra di gestione delle promozioni degli utenti
 */
public class NominaGiudice extends DialogBox
{
  interface NominaGiudiceBinder extends UiBinder<Widget, NominaGiudice> {}
  private static NominaGiudiceBinder uiBinder = GWT.create(NominaGiudiceBinder.class);

  @UiField HTMLPanel registeredUsersListContainer;
  
  private ClasseControllo controller;
  
  public NominaGiudice(ClasseControllo controller, ArrayList<User> registeredUsersList) 
  {
    this.controller = controller;
    
    setWidget(uiBinder.createAndBindUi(this));
    
    showUsers(registeredUsersList);
    
    setStyleName("dialog");
    
    this.setAutoHideEnabled(true);
    this.show();
    centerDialog();
  }

  //Metodo per centrare orizzontalmente la finestra
  private void centerDialog()
  {
    this.setPopupPosition(((Window.getClientWidth() - getOffsetWidth())/2), 100);
  }
  
  //Metodo che aggiorna l'elenco degli utenti registrati, ma non giudici
  public void refreshJudges(ArrayList<User> registered)
  {
    registeredUsersListContainer.clear();
    showUsers(registered);
  }

  //Metodo che visualizza l'elenco degli utenti registrati, ma non giudici
  private void showUsers(ArrayList<User> registeredUsersList)
  {
      }
  
  
}
