package faq.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

/**
 * Classe che gestisce l'avvio dell'applicazione
 */
public class FAQ implements EntryPoint
{
  private ClasseControllo controller = GWT.create(ClasseControllo.class);
  @Override
  public void onModuleLoad()
  {
    controller.init();
  }
}

