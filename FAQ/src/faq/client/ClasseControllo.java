package faq.client;


import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.core.client.GWT;

import com.google.gwt.user.client.rpc.AsyncCallback;


import faq.shared.*;
import faq.client.FAQService;
import faq.client.FAQServiceAsync;
import faq.client.GUIHandler;

/**
 * Classe che gestisce la comunicazione con il server e 
 * le conseguenti richieste di modifica all'interfaccia grafica
 */
public class ClasseControllo {
	
	private FAQServiceAsync service;
	private GUIHandler guiHandler;
	
	/*
	 * Costruttore della classe: quando invocato
	 * L'istanza del Server (DRService Async)
	 * L'istanza della classe adibita alla gestione dell'interfaccia utente (GUIHandler)
	 */
	public ClasseControllo()
	{
	  service = GWT.create(FAQService.class);
	  guiHandler = new GUIHandler(this);
	}
	
	/* 
	 * Metodo per gestire l'avvio dell'applicazione:
	 * Creazione dell'header dell'applicazione
	 * Richiesta al server dell'elenco delle domande
	 */
	public void init()
	{
	  guiHandler.createHeader();
	  requestQuestions(null); 
	}
	
	//Metodo per creare una finestra di avvertimento
	public void alertFailure(String alertMessage)
	  {
	    guiHandler.createWidget("alert",alertMessage);
	  }
		
		//Metodo per gestire l'evento di login e logout
		public void setLogged(User user)
	  {
	    if(user != null) //Evento di login
	    {
	      guiHandler.handleLogRequest(true); //Viene mostrato solo il bottone di logout
	    	if(user instanceof AdminUser) //Creazione dell'interfaccia utente in funzione del ruolo dell'utente
	    	{
	    	  createAdminUI();
	    	} else
	    	{
	    	  createRegisteredUI();
	    	}
	    } else //Evento di logout
	    {
	      requestQuestions(null);
	    }
	  }
		
	/*
	 * Metodo per gestire la richiesta di login:
	 * Se la richiesta va a buon fine, viene avvertito l'utente dell'avvenuto login e richiamato il metodo per gestire l'evento di login
	 * Se le richiesta non va a buon fine, viene avvertito l'utente del problema riscontrato
	 */
	public void login( String username, String password )
		{
		  //Richiesta di login al server 
		  service.login(username, password, new AsyncCallback<User>() //Invocazione del metodo di Login del server
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        try
	        {
	          throw caught; //Viene lanciata l'eccezione restituita dal server per poter evidenziare quale è stato il problema
	        } catch (WrongPasswordException wp)
	        {
	          guiHandler.cleanFields("login");
	          alertFailure("Password inserita errata");
	        } catch(UserNotFoundException e)
	        {
	          guiHandler.cleanFields("login");
	          alertFailure("Utente non trovato nel database");
	        }
	        catch( Throwable e) //Problema generico
	        {
	          guiHandler.hideDialog();
	          alertFailure("Non e' stato possibile effettuare il login");
	        }
	      }
	    
	      @Override
	      public void onSuccess(User returnedUser) 
	      {
	        guiHandler.hideDialog();
	        guiHandler.createWidget("notify", "Login effettuato con successo"); //Viene avvertito l'utente che il login � stato effettuato con successo
	        
	      }
			});
		}
	
		/*
	   * Metodo per gestire la richiesta di registrazione:
	   * Se la richiesta va a buon fine, viene avvertito l'utente dell'avvenuta registrazione e richiamato il metodo per gestire l'evento di login
	   * Se le richiesta non va a buon fine, viene avvertito l'utente del problema riscontrato
	   */
	  public void registration
	    (
	      String username, 
	      String password, 
	      String email, 
	      String name, 
	      String lastname, 
	      String gender, 
	      String birthdate, 
	      String birthplace, 
	      String address
	    )
	  {
	    //Richiesta al server della registrazione del nuovo utente
	    service.addUser(username, password, email, name, lastname, gender, birthdate, birthplace, address, new AsyncCallback<User>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        try
	        { 
	          throw caught; //Viene lanciata l'eccezione restituita dal server per poter evidenziare quale è stato il problema
	        } catch ( DuplicatedUserNameException du )
	        {
	          guiHandler.cleanFields("registration");
	          alertFailure("Attenzione l'username " + du.getUserName() + " non puo' essere scelto");
	        } catch(Throwable e) //Problema generico
	        {
	          guiHandler.hideDialog(); 
	          alertFailure("Non e' stato possibile effettuare la registrazione");
	        }
	      }
	    
	      @Override
	      public void onSuccess(User returnedUser)
	      {
	        guiHandler.hideDialog();
	        guiHandler.createWidget("notify", "Registrazione effettuata con successo"); //Viene avvertito l'utente che la registazione è stata effettutata con successo
	        
	      }
	    });
	  }
		  
	  /*
	   * Metodo per creare l'interfaccia grafica dell'utente registrato/giudice
	   */
	  private void createRegisteredUI()
	  {
	    AsyncCallback<Category> categoryRequestAsyncCallback = new AsyncCallback<Category>()
	    {
	  
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile ottenere l'elenco delle categorie");
	      }
	      
	      @Override
	      public void onSuccess(Category result)
	      {
	        if(result != null)
	        {
	          guiHandler.createWidget("categoryBoard",result); //Creazione della sezione per la selezione delle domande per categoria
	          guiHandler.createWidget("question", result); //Creazione della sezione per l'inserimento di una nuova domanda
	        } else 
	        {
	          alertFailure("Nessuna categoria disponibile");
	        }
	      }
	    };
	    service.getAllCategory(categoryRequestAsyncCallback); //Richiesta al server dell'albero delle categorie
	  }
	  
	  /*
	   * Metodo per richiedere la lista delle domande:
	   * Se la richiesta va a buon fine viene mostrato l'elenco delle domande restituito dal server
	   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
	   */
	  public void requestQuestions(Category categories)
	  {
	    AsyncCallback<ArrayList<Question>> questionRequestAsyncCallback = new AsyncCallback<ArrayList<Question>>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile ottenere l'elenco delle domande");
	      }
	      
	      @Override
	      public void onSuccess(ArrayList<Question> questions)
	      {
	        showQuestions(questions, null);
	      }
	    };
	    
	    if(categories != null) 
	    {
	      service.getAllQuestions(questionRequestAsyncCallback); //Richiesta al server dell'elenco di tutte le domande
	    } else {
	      service.getQuestions(categories, questionRequestAsyncCallback); //Richiesta al server dell'elenco di tutte le domande appartenenti a una categoria
	    }
	  }
	  
	  /*
	   * Metodo per mostrare l'elenco delle domande nella colonna centrale
	   * della pagina in funzione del ruolo dell'utente
	   */
	  private void showQuestions(ArrayList<Question> questions, User user)
	  {
	    guiHandler.clearQuestionColumn();
	    if(user != null && user instanceof AdminUser)
	    {
	      guiHandler.showAdminQuestions(questions);
	    }
	    else
	    {
	      guiHandler.showQuestions(questions);
	    }
	  }
	  
	  /*
	   * Metodo per richiedere l'inserimento di una nuova domanda:
	   * Se la richiesta va a buon fine viene aggiornato l'elenco delle domande restituito dal server
	   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
	   */
	  public void addNewQuestion(String question, RegisteredUser user, Category category)
	  {
	    AsyncCallback<ArrayList<Question>> addNewQuestion = new AsyncCallback<ArrayList<Question>>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        
	        alertFailure("Non � stato possibile inserire la domanda");
	      }
	      
	      @Override
	      public void onSuccess(ArrayList<Question> questions)
	      {
	        showQuestions(questions, null);
	      }
	    };
	    service.addQuestion(category, new Question(question, user), addNewQuestion);
	  }
	  
	  /*
	   * Metodo che richiede al server una domanda specifica:
	   * Se la richiesta va a buon fine viene mostrata la finestra contenente la domanda specifica
	   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
	   */
	  public void extendedQuestionDialog(Category category, String question, User user)
	  {
	    AsyncCallback<Question> requestFullQuestion = new AsyncCallback<Question>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile ottenere la domanda");
	      }
	      
	      @Override
	      public void onSuccess(Question question)
	      {
	        showQuestionDialog(question, user);
	      }
	    };
	    service.getQuestions(category, question, requestFullQuestion); //Richiesta al server di una domanda specifica
	  }
	  
	//Metodo che crea la finestra di una specifica domanda
	  private void showQuestionDialog(Question question, User user)
	  {
	    if(user != null)
	    {
	      guiHandler.showQuestionDialog(question, user);
	    } else
	    {
	      guiHandler.showQuestionDialog(question, UnregisteredUser.get());
	    } 
	  }
	  
	  /*
	   * Metodo per richiedere l'inserimento di una nuova risposta
	   * Se la richiesta va a buon fine viene aggiornato l'elenco delle risposte nella finestra della domanda specifica
	   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
	   */
	  public void addNewAnswer(Question question, RegisteredUser user, String answer )
	  {
	    AsyncCallback<Question> addNewAnswerCallback = new AsyncCallback<Question>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        guiHandler.cleanFields("answer");
	        alertFailure("Non � stato possibile inserire la risposta");
	      }
	      
	      @Override
	      public void onSuccess(Question answer)
	      {
	        guiHandler.cleanFields("answer");
	        //guiHandler.updateQuestionDialog(question, answer);
	      }
	    };
	    service.addAnswer(new Answer(answer, user), question, addNewAnswerCallback); //Richiesta al server di aggiunta della risposta
	  }
	  
	  /*
	   * Metodo per aggiungere un nuovo giudizio a una risposta:
	   * Se la richiesta va a buon fine viene aggiornato il giudizio medio della risposta nella finestra della domanda specifica
	   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
	   */
	  public void addJudgment(JudgeUser user, Answer answer, int value)
	  {
	    AsyncCallback<Answer> addNewJudgementCallback = new AsyncCallback<Answer>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile aggiungere il giudizio");
	      }
	      
	      @Override
	      public void onSuccess(Answer answer)
	      {
	        guiHandler.updateQuestionDialog(answer, user);
	      }
	    };
	    
	    //Contollo che il giudice non stia giudicando una propria risposta o stia esprimendo un duplice giudizio
	    if(answer.getUser().getUsername().equals(user.getUsername()))
	    {
	    	Iterator sfogliaGiudizi  =answer.createIterator();
	    	while(sfogliaGiudizi.hasNext()){
	    		if(user.getUsername().equals(((Judgment) sfogliaGiudizi.next()).getUser().getUsername()))
	  	      {
	    			alertFailure("Non � consentito esprimere molteplici giudizi sulla stessa risposta");
	  	        
	  	      } 
	    	}
	    	//Richiesta al server di aggiunta del giudizio alla risposta
  	        service.addJudgment(answer, new Judgment(value, user), addNewJudgementCallback);
	      
	    } else
	    {
	      alertFailure("Non � consentito esprimere un giudizio su una propria risposta");
	    }
	  }
		  
	  /*
	   * Metodo che richiede l'eliminazione di una risposta:
	   * Se la richiesta va a buon fine viene aggiornato l'elenco delle risposte nella finestra della domanda specifica
	   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
	   */
	  public void deleteAnswer(Question question, Answer answer, User user)
	  {
	    AsyncCallback<Question> deleteAnswerCallback = new AsyncCallback<Question>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile eliminare la risposta");
	      }
	      
	      @Override
	      public void onSuccess(Question question)
	      {
	        guiHandler.updateQuestionDialog(question, user);
	      }
	    };
	    service.removeAnswer(question, user, answer,deleteAnswerCallback); //Richiesta al server di rimozione di una risposta
	  }
	  
	  /*
	   * Metodo per richiedere l'eliminazione di una domanda
	   * Se la richiesta va a buon fine viene aggiornato l'elenco delle domande
	   * Se la richiesta non va a buon fine viene avvertito l'utente del problema
	   */
	  public void deleteQuestion(Category category, Question question, User user)
	  {
	    AsyncCallback<ArrayList<Question>> deleteQuestionCallback = new AsyncCallback<ArrayList<Question>>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile eliminare la domanda");
	      }
	      
	      @Override
	      public void onSuccess(ArrayList<Question> questions)
	      {
	        showQuestions(questions, null);
	      }
	    };
	    service.removeQuestion(category, question, user, deleteQuestionCallback); //Richiesta al server di rimozione di una domanda
	  }
	  
	//Metodo che genera l'interfaccia utente dell'Amministratore
	  private void createAdminUI()
	  {
	    //Creazione dell'elenco delle domande per l'Amministratore
	    requestQuestions(null);
	    //Creazione pannello per la gestione delle categorie e per la gestione del ruolo degli utenti
	    guiHandler.createWidget("admin", null);
	  }
	  
	  /*
	   * Metodo per richiedere l'elenco delle categorie:
	   * Se la richiesta va a buon fine:
	   *  se l'elenco delle categorie non � vuoto viene mostrata la finestra di gestione delle categorie
	   *  se l'elenco delle categorie � vuoto viene avvertito l'Amministratore
	   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
	   */
	  public void getManagementCategory()
	  {
	    AsyncCallback<Category> categoryRequestAsyncCallback = new AsyncCallback<Category>()
	    {

	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile ottenere l'elenco delle categorie");
	      }
	      
	      @Override
	      public void onSuccess(Category result)
	      {
	        if(result != null)
	        {
	          guiHandler.createWidget("categoryManagement", result);
	        } else 
	        {
	          alertFailure("Nessuna categoria disponibile.");
	        }
	      }
	     };
	     
	    service.getAllCategory(categoryRequestAsyncCallback); //Richiesta al server dell'albero delle categorie
	  }
	  

	  /*
	   * Metodo per rinominare una categoria:
	   * Se la richiesta va a buon fine viene aggiornato l'elenco delle categorie nella finestra di gestione delle categorie
	   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
	   */
	  public void renameCategory(Category category, String Name)
	  {
	    AsyncCallback<Category> renameCategoryCallback = new AsyncCallback<Category>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile rinominare la categoria");
	      }
	      
	      @Override
	      public void onSuccess(Category category)
	      {
	        guiHandler.updateCategoryList(category);
	      }
	    };
	    service.renameCategory(category, Name, renameCategoryCallback); //Richiesta al server di rinominare la categoria
	  }
	  
	  /*
	   * Metodo per aggiungere una categoria:
	   * Se la richiesta va a buon fine viene aggiornato l'elenco delle categorie nella finestra di gestione delle categorie
	   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
	   */
	  public void addCategory(String name)
	  {
	    AsyncCallback<Category> addCategoryCallback = new AsyncCallback<Category>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile aggiungere la categoria");
	      }
	      
	      @Override
	      public void onSuccess(Category category)
	      {
	        guiHandler.updateCategoryList(category);
	      }
	    };
	    service.addCategory(name, addCategoryCallback);
	  }
	  
	  /*
	   * Metodo per aggiungere una sottocategoria a una categoria esistente
	   * Se la richiesta va a buon fine viene aggiornato l'elenco delle categorie nella finestra di gestione delle categorie
	   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
	   */  
	  public void addSubCategory(Category category, String name)
	  {
	    AsyncCallback<Category> addCategoryCallback = new AsyncCallback<Category>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile aggiungere la sotto categoria");
	      }
	      
	      @Override
	      public void onSuccess(Category category)
	      {
	        guiHandler.updateCategoryList(category);
	      }
	    };
	    service.addSubCategory(category, name, addCategoryCallback);
	  }
	  
	  /*
	   * Metodo per richiedere l'elenco degli utenti registrati ma non giudici
	   * Se la richiesta va a buon fine viene creata la finestra di gestione delle promozioni
	   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
	   */
	  public void getRegisteredUsersBoard()
	  {
	    AsyncCallback<ArrayList<User>> getRegisteredUsersCallback = new AsyncCallback<ArrayList<User>>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile ottenere l'elenco degli utenti registrati");
	      }
	      
	      @Override
	      public void onSuccess(ArrayList<User> registeredUsers)
	      {
	        if(registeredUsers.size() != 0)
	        {
	          guiHandler.createWidget("judges", registeredUsers);;
	        } else
	        {
	          alertFailure("Attenzione tutti gli utenti registrati sono dei giudici");
	        }
	      }
	    };
	    service.getRegistered(getRegisteredUsersCallback); //Richiesta al server dell'elenco degli utenti registrati, ma non giudici
	  }
	  
	  /*
	   * Metodo per nominare un nuovo giudice:
	   *Se la richiesta va a buon fine viene aggiornato l'elenco degli utenti registrati, ma non giudici
	   * Se la richiesta non va a buon fine viene avvertito l'Amministratore del problema
	   */
	  public void nominateJudge(String username)
	  {
	    final String user = username;
	    AsyncCallback<ArrayList<User>> nominateJudgeCallback = new AsyncCallback<ArrayList<User>>()
	    {
	      @Override
	      public void onFailure(Throwable caught)
	      {
	        alertFailure("Non � stato possibile rendere "+user+" un nuovo giudice");
	      }
	      
	      @Override
	      public void onSuccess(ArrayList<User> registered)
	      {
	        if(registered.size() != 0)
	        {
	          guiHandler.updateJudges(registered);
	        } else
	        {
	          guiHandler.hideDialog();
	          alertFailure("Attenzione tutti gli utenti registrati sono dei giudici");
	        }
	      }
	    };
	    service.nominateJudge(username, nominateJudgeCallback); //Richiesta al server di nomina del giudice
	  }
}

 
  



 
  

