package faq.client;

import java.util.Iterator;



import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import faq.shared.Answer;

import faq.shared.JudgeUser;
import faq.shared.Question;
import faq.shared.RegisteredUser;
import faq.shared.User;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * la visualizzazione di una domanda specifica
 */
public class QuestionDialog extends DialogBox
{
  interface QuestionDialogBinder extends UiBinder<Widget, QuestionDialog> {}
  private static QuestionDialogBinder uiBinder = GWT.create(QuestionDialogBinder.class);
  
  @UiField Label category;
  @UiField Label date;
  @UiField Label questionText;
  @UiField VerticalPanel answers;
  @UiField HTMLPanel questionDialog;
  
  private ClasseControllo controller;
  private Answer answer;
  private Question question;
  private NewAnswer newAnswer;
  
  public QuestionDialog(ClasseControllo controller, Question question, Answer answer) 
  {
    this.controller = controller;
    this.question = question;
    this.answer = answer;
    
    setWidget(uiBinder.createAndBindUi(this)); 
    
    category.setText(question.getName());
    date.setText(question.getDay() + "  " + question.getTime());
    questionText.setText(question.getQuestion());

    setStyleNames();
    
    this.setAutoHideEnabled(true);
    this.show();
  }
  
  private void setStyleNames(){
	  setStyleName("dialog");
	  
	  category.getElement().setId("label");
	  date.getElement().setId("dateText");
	  questionText.getElement().setId("dialogTextElem");
  }

  //Metodo per centrare orizzontalmente la finestra
  public void centerDialog()
  {
    this.setPopupPosition(((Window.getClientWidth() - getOffsetWidth())/2), 100);
  }

  //Metodo che visualizza lo spazio per l'inserimento di una nuova risposta
  public void showNewAnswer()
  {
    newAnswer = new NewAnswer(this);
    questionDialog.add(newAnswer);
  }
  
  //Metodo che visualizza le risposte associate alla domanda
  public void showAnswers()
  {
    if(question!=null)
    {
    	Iterator<?> sfogliaRisposte = question.createIterator();
      while(sfogliaRisposte.hasNext())
      {
        SingleAnswer answer = new SingleAnswer(this, (Answer) sfogliaRisposte.next());
        answers.add(answer);
      }
    }
  }
  
  //Metodo che visualizza le risposte associate alla domanda
  public void showJudgeAnswers()
  {
    if(question!=null)
    {
    	Iterator<?> sfogliaRisposte = question.createIterator();
      while(sfogliaRisposte.hasNext())
      {
        SingleAnswer answer = new SingleAnswer(this, (Answer) sfogliaRisposte.next());
        answer.addJudgeFunction();
        answers.add(answer);
      }
    }
  }
  
  //Metodo che visualizza le risposte associate alla domanda
  public void showAdminAnswers()
  {
	  if(question!=null)
	    {
	    	Iterator<?> sfogliaRisposte = question.createIterator();
	      while(sfogliaRisposte.hasNext())
	      {
	    	  SingleAnswer answer = new SingleAnswer(this, (Answer) sfogliaRisposte.next());
		        answer.addAdminFunction();
		        answers.add(answer);
	      }
    }
  }
  
  //Metodo per l'aggiornamento della domanda di riferimento dell'oggetto
  public void updateDialog(Answer answer, Question question)
  {
    this.question = question;
    this.answer = answer;
    answers.clear();
  }
  
  //Metodo che richiama il metodo per inserire una nuova risposta
  public void addAnswer(String text, RegisteredUser user )
  {
    controller.addNewAnswer(question, user, text);
  }
  
  //Metodo che richiama il metodo per inserire un nuovo giudizio
  public void addJudgment(Answer answer, int value, JudgeUser user)
  {
    controller.addJudgment( user, answer, value);
  }
  
  //Metodo che richiama il metodo per cancellare una domanda
  public void deleteAnswer(Answer answer, User user )
  {
    controller.deleteAnswer(question, answer, user);
  }
  
  //Metodo per ripulire la TextArea per l'inserimento di una nuova risposta
  public void clearTextArea()
  {
   newAnswer.clearTextArea(); 
  }
  
  //Metodo per creare una finestra di avvertimento
  public void alertFailure(String alertMessage)
  {
    controller.alertFailure(alertMessage);
  }
}
