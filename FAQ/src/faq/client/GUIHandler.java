package faq.client;


import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;


import faq.client.*;
import faq.shared.*;
import faq.shared.Question;


/**
 * Classe adibita alla gestione degli elementi dell'interfaccia grafica
 */
public class GUIHandler
{
  private ClasseControllo controller;
  private Header header;
  private Widget widget;
  
  public GUIHandler(ClasseControllo controller)
  {
    this.controller = controller;
  }
  
  /*
   * Metodo per la creazione dell'header della pagina
   */
  public void createHeader()
  {
    header = new Header(this);
  }
  /*
   * Metodo per la gestione dell'evento di login/logout
   */
  public void handleLogRequest(boolean isLogin)
  {
    header.swapLogButtons(isLogin);
    if(!isLogin)
    {
      clearHP();
      controller.setLogged(null);
    }
  }
  
  public void clearHP()
  {
    RootPanel.get("Categories").clear(); //Rimozione della finestra di selezione delle categorie dalla pagina
    RootPanel.get("RightSideBar").clear(); //Rimozione della finestra di inserimento di una nuova domanda dalla pagina
  }
  
  /*
   * Metodo per la creazione dei vari componenti dell'interfaccia
   */
  @SuppressWarnings ( "unchecked")
public void createWidget(String type, Object object)
  {
    switch (type)
    {
    case "login": //Creazione della finestra di login
      widget = new ControlloLogin(controller);
      break;
    case "registration": //Creazione della finestra di registrazione
      widget = new ControlloRegistrazione(controller);
      break;
    case "categoryBoard": //Creazione della sezione per la selezione delle categorie
      new CategoryBoard(controller, (Category)object);
      break;
    case "question": //Creazione della sezione per l'inserimento di una nuova domanda
      RootPanel.get("RightSideBar").add(new NewQuestion(controller, generateCategoryNameList((Category)object)));
      break;
    case "questionExtended": //Creazione della finestra per la visualizzazione di una domanda specifica
      widget = new QuestionDialog(controller,(Question)object, null);
      break;
    case "admin": //Creazione della sezione delle funzioni dell'Amministratore
      RootPanel.get("RightSideBar").add(new AdminFunctionSelection(controller));
      break;
    case "categoryManagement": //Creazione della finestra di gestione delle categorie
      widget = new CategoryManagement(controller, this, generateCategoryNameList((Category)object));
      break;
    case "judges": //Creazione della finestra di gestione delle promozioni
      widget = new JudgeNominationBoard(controller, (ArrayList<User>)object);
      break;
    default: //Creazione di una finestra di dialogo generica (alert/notify)
      new NotificaAvviso(type, (String)object);
      break;
    }
  }
  
  /*
   * Metodo per ripulire i campi di testo dei vari widget
   */
  public void cleanFields(String piece)
  {
    switch (piece)
    {
      case "loging":
        ((ControlloLogin) widget).cleanFields();
        break;
      case "registration":
        ((ControlloRegistrazione) widget).cleanFields();
        break;
      case"answer":
        ((QuestionDialog) widget).clearTextArea();
        break;
    }
  }
  
  public void hideDialog()
  {
    ((DialogBox)widget).hide();
  }
  
  public void clearQuestionColumn()
  {
    if(RootPanel.get("Questions").getWidgetCount() != 0)
    {
      RootPanel.get("Questions").clear();
    }
  }
  
  /*
   * Metodo per la visualizzazione delle domande nella colonna centrale dell'HomePage
   */
  public void showQuestions(ArrayList<Question> questions)
  {
    for(Question q:questions)
    {
      QuestionButton qB = new QuestionButton(controller, q);
      RootPanel.get("Questions").add(qB);
    }
  }
  
  /*
   * Metodo per la visualizzazione delle domande specifiche dell'admin nella colonna centrale dell'HomePage
   */
  public void showAdminQuestions(ArrayList<Question> questions)
  {
    for(Question q:questions)
    {
      AdminQuestion qB = new AdminQuestion(controller, q);
      RootPanel.get("Questions").add(qB);
    }
  }
  
  /*
   * Metodo per la visualizzazione della finestra di una specifica domanda
   */
  public void showQuestionDialog(Question question, User user)
  {
    createWidget("questionExteded", question);
    setNewAnswer(user); //Aggiunta della sezione per l'inserimento di una nuova risposta
    setAnswers(user);
    ((QuestionDialog)widget).centerDialog();
  }
  
  /*
   * Metodo per l'aggiunta della sezione per l'inserimento di una nuova risposta
   */
  private void setNewAnswer(User user)
  {
    if(user instanceof AdminUser || user instanceof JudgeUser)
    {
      ((QuestionDialog)widget).showNewAnswer();
    }
  }
  
  /*
   * Metodo che imposta l'interfaccia delle risposte nella finestra di una 
   * specifica domanda in funzione del ruolo dell'utente
   */
  private void setAnswers(User user)
  {
    if (user instanceof JudgeUser)
    {
        ((QuestionDialog)widget).showJudgeAnswers();
    }
    if (user instanceof AdminUser)
    {
        ((QuestionDialog)widget).showAdminAnswers();
    }
    if (user instanceof UnregisteredUser)
    {
        ((QuestionDialog)widget).showAnswers();
    }
  }
  
  /*
   * Metodo per aggiornare l'elenco delle risposte nella finestra di una specifica domanda
   */
  public void updateQuestionDialog(Answer answer, User user)
  {
    ((QuestionDialog)widget).updateDialog(answer, null);
    setAnswers(user);
    ((QuestionDialog)widget).centerDialog();
  }
  
  /*
   * Metodo per aggiornare l'elenco delle categorie nella finestra di gestione delle categorie
   */
  public void updateCategoryList(Category category)
  {
    ((CategoryManagement)widget).refreshCategory(generateCategoryNameList(category));
  }
  
  /*
   * Metodo per aggiornare l'elenco degli utenti registrati nella finestra di gestione delle promozioni
   */
  public void updateJudges(ArrayList<User> registered)
  {
    ((JudgeNominationBoard)widget).refreshJudges(registered);
  }
  
  /*
   * Metodo per creare la lista delle categorie indentate
   */
  public List<String[]> generateCategoryNameList(Category categoryTree)
  {
    List<String[]> categoryNameList = new ArrayList<>();
    categoryNameList = returnPrintableTree(categoryTree, 0, categoryNameList);
    return categoryNameList;
  }
  
  /* Metodo che ritorna la lista delle categorie indentate in funzione della posizione all'interno dell'albero
   * Parametri:
   * - current: oggetto categoria
   * - level: indicazione del livello dell'albero in cui si trova l'oggetto categoria
   * - printableTree: oggetto che contiene tutto l'elenco delle categorie formattate per la stampa
   * Return:
   * - List<String[]>: lista di coppie IdCategoria - NomeCategoria 
   */
  private List<String[]> returnPrintableTree(CategoryComponent current, int level, List<String[]> printableTree)
  {
	  
    if(current != null) 
    {
    	if (current instanceof Category)
    	{
    		printableTree.add(printableLevel(current, level));
    		level = level+1;
    	    CategoryIterator sfogliaCategoria = (CategoryIterator) current.createIterator();
    	    while(sfogliaCategoria.hasNext())
    	     {
    	        returnPrintableTree((CategoryComponent) sfogliaCategoria.next(), level, printableTree);
    	     }
    	}
    } else 
    {
      printableTree.add(printableLevel(current, level));
      return printableTree;
    }
    return printableTree;
  }
  
  /*
   * Metodo che inserisce l'indentazione in funzione del livello della categoria
   * Restituisce un array contenente il nome della categoria
   * indentato in funzione del livello
   */
  private String[] printableLevel(CategoryComponent category, int level)
  {
    String[] currentCategoryWithId = new String[2];
    String currentCategory = "";
    for(int i=0; i<level; i++)
    {
      currentCategory = currentCategory + "- ";
    }
    currentCategoryWithId[0] = ""+category.getName();
    currentCategoryWithId[1] = currentCategory + category.getName();
    return currentCategoryWithId;
  }
  
  
  
  
  
  

  




 









}
