package faq.client;

import com.google.gwt.user.client.ui.TreeItem;

import faq.shared.Category;

public class CategoryItem extends TreeItem{
	
	private String name;
	
	public CategoryItem(Category c){
		if(c != null){
			this.name = c.getName();
			setText(c.getName());
		} else {
			setText("Categorie");
		}
	}
	
	public String getName(){
		return name;
	}	
}
