package faq.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import faq.shared.Answer;
import faq.shared.Category;
import faq.shared.DuplicatedUserNameException;
import faq.shared.Judgment;
import faq.shared.Question;
import faq.shared.RegisteredUser;
import faq.shared.User;
import faq.shared.UserNotFoundException;
import faq.shared.WrongPasswordException;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("FAQ")
public interface FAQService extends RemoteService {
	
	User addUser( String username, String password, String email, String name, String surname, String sex, String birthDate, String birthPlace, String address )throws DuplicatedUserNameException;
	User login( String userName, String password)throws WrongPasswordException, UserNotFoundException;
	ArrayList<User> getAllUsers();	
	ArrayList<User> getJudges();	
	ArrayList<User> getRegistered();	
	ArrayList<User> nominateJudge( String userName );
	Question removeAnswer(Question question, User user, Answer answer);
	Category renameCategory(Category category, String name);
	Category addCategory(String name);
	Category addSubCategory(Category category, String name);
	Answer addJudgment(Answer answer, Judgment judgment);
	ArrayList<Question> addQuestion(Category category, Question question);
	ArrayList<Question> getQuestions(Category category);
	ArrayList<Question> removeQuestion(Category category, Question question, User user);
	Question getQuestions(Category category, String question);
	Category getAllCategory();
	
	Question addAnswer(Answer answer, Question question);
	ArrayList<Question>getAllQuestions();
	
	
	
}
