package faq.client;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;

import faq.shared.Category;
import faq.shared.CategoryComponent;
import faq.shared.CategoryIterator;

public class CategoryBoard extends Composite
{

  @UiField Tree staticTree;

  private ClasseControllo controller;

  public CategoryBoard(ClasseControllo controller, Category categoryTree)
  {
    this.controller = controller;
    drawCategoryTree(categoryTree);
  }

  private void drawCategoryTree(Category categoryTree)
  {
    staticTree = createStaticTree(categoryTree);
    staticTree.setAnimationEnabled(true);

    // Wrap the static tree in a scrollPanel
    //ScrollPanel staticTreeWrapper = new ScrollPanel(staticTree);
    //staticTreeWrapper.setSize("300px", "300px");

    // Wrap the static tree in a VerticalPanel
    VerticalPanel panel = new VerticalPanel();
    panel.add(staticTree);

    //Adding onClick event
    addHandler();
    
    RootPanel.get("Categories").add(panel);
  }

  private void addHandler()
  {
	  staticTree.addSelectionHandler(new SelectionHandler<TreeItem>(){
		@Override
		public void onSelection(SelectionEvent<TreeItem> event) {
			CategoryItem selected = (CategoryItem) event.getSelectedItem();
			controller.requestQuestions(null);
		}
	  });
  }

  private Tree createStaticTree(Category current)
  {
    Tree staticTree = new Tree();

    if(current!=null)
	{
		CategoryIterator sfogliaCategoria = (CategoryIterator) current.createIterator();
		while(sfogliaCategoria.hasNext())
		{
			CategoryComponent componente = (CategoryComponent) sfogliaCategoria.next();
			if (componente instanceof Category)
			{
				CategoryItem child = new CategoryItem((Category) componente);
				staticTree.addItem(child);
				createStaticTree((Category) componente);
			}
		}
	}
	
    // Return the tree
    return staticTree;
  }

  private CategoryItem visit(Category current, CategoryItem father)
  {
	if(current!=null)
	{
		CategoryIterator sfogliaCategoria = (CategoryIterator) current.createIterator();
		while(sfogliaCategoria.hasNext())
		{
			CategoryComponent componente = (CategoryComponent) sfogliaCategoria.next();
			if (componente instanceof Category)
			{
				CategoryItem child = new CategoryItem((Category) componente);
		        father.addItem(child);
				visit((Category) componente, father);
			}
		}
	}
    return father;
  }
}
