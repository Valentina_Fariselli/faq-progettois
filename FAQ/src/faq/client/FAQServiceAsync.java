package faq.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import faq.shared.Answer;
import faq.shared.Category;
import faq.shared.Judgment;
import faq.shared.Question;
import faq.shared.RegisteredUser;
import faq.shared.User;

/**
 * The async counterpart of <code></code>.
 */
public interface FAQServiceAsync {
	
	void login(String userName, String password, AsyncCallback<User> callback);

	void addUser(String username, String password, String email, String name, String surname, String sex,
			String birthDate, String birthPlace, String address, AsyncCallback<User> callback);

	void getAllUsers(AsyncCallback<ArrayList<User>> callback);

	void getJudges( AsyncCallback<ArrayList<User>> callback );

	void getRegistered( AsyncCallback<ArrayList<User>> callback );

	void nominateJudge( String userName, AsyncCallback<ArrayList<User>> callback );

	void addJudgment(Answer answer, Judgment judgment, AsyncCallback<Answer> addNewJudgementCallback);

	void removeAnswer(Question question, User user, Answer answer,
			AsyncCallback<Question> deleteAnswerCallback);

	void removeQuestion(Category category, Question question,
			User user, AsyncCallback<ArrayList<Question>> deleteQuestionCallback);

	void renameCategory(Category category, String name, AsyncCallback<Category> renameCategoryCallback);

	void addCategory(String name, AsyncCallback<Category> addCategoryCallback);

	void addSubCategory(Category category, String name, AsyncCallback<Category> addCategoryCallback);

	void getQuestions(Category category, AsyncCallback<ArrayList<Question>> questionRequestAsyncCallback);

	void addQuestion(Category category, Question question, 
			AsyncCallback<ArrayList<Question>> addNewQuestion);

	void getQuestions(Category category, String question, AsyncCallback<Question> requestFullQuestion);

	void getAllCategory(AsyncCallback<Category> categoryRequestAsyncCallback);

	void addAnswer(Answer answer, Question question, AsyncCallback<Question> addNewAnswerCallback);

	void getAllQuestions(AsyncCallback<ArrayList<Question>> callback);

	

	
	
}
