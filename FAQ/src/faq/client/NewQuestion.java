package faq.client;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

import faq.shared.RegisteredUser;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * l'inserimento di una nuova domanda
 */
public class NewQuestion extends Composite
{
  interface NewQuestionBinder extends UiBinder<Widget, NewQuestion> {}
  private static NewQuestionBinder uiBinder = GWT.create(NewQuestionBinder.class);

  private ClasseControllo controller;
  private List<String[]> categoryMap; //Lista di coppie
  
  @UiField HTMLPanel newQuestionContainer;
  @UiField Label box_title;
  @UiField ListBox categoryList;
  @UiField TextArea questionText;
  @UiField Button sendQuestion;
  @UiField Button clear;
  
  public NewQuestion(ClasseControllo controller, List<String[]> categoryMap)
  {
  	this.controller = controller;
    this.categoryMap = categoryMap;
  	
    initWidget(uiBinder.createAndBindUi(this));
    
    categoryList.setVisibleItemCount(1);
    for(String[] s:categoryMap)
    {
      categoryList.addItem(s[1]);
    }
    
    setStyleName("rightBox");
    box_title.setStyleName("box_title");
    categoryList.setStyleName("rightBoxElem");
    questionText.setStyleName("rightBoxElem");
    questionText.getElement().setPropertyString("placeholder", "Inserisci una nuova domanda...");
	}
  
  //Metodo che controlla il testo contenuto della TextArea in cui l'utente inserisce il testo della domanda
  private boolean checkQuestionText()
  {
    if(questionText.getText().length() == 0)
    {
      controller.alertFailure("Prego inserire il testo della domanda");
      return false;
    } else if(questionText.getText().length() > 300)
    {
      controller.alertFailure("La lunghezza della domanda non pu� superare i 300 caratteri");
      questionText.setText("");
      return false;
    } 
    return true;
  }
  
  //Metodo che controlla che sia stata selezionata una categoria da associare alla domanda
  private boolean isCategorySelected()
  {
    return categoryList.getSelectedIndex() != 0;
  }
  
  /*
   * Associo il clickHandler al Button attraverso l'UiHandler:
   * - sendQuestion:
   *    se � stato inserito il testo della domanda e la categoria cui appartiene allora � richiesta l'aggiunta di una nuova domanda
   *    se non e stato inserito il testo della domdna o non � stata selezionata la categoria di appartenenza viene avvertito l'utente
   * - clear: ripulisce la sezione
   */
  @UiHandler(value={"sendQuestion", "clear"})
  void handleClick(ClickEvent event) {
    Widget sender = (Widget) event.getSource();
    if(sender == sendQuestion)
    {
      if(checkQuestionText())
      {
        if(isCategorySelected())
        {
          String category = categoryMap.get(categoryList.getSelectedIndex())[0];
          String text = questionText.getText();
          controller.addNewQuestion(text,category);
          clear();
        } else
        {
          controller.alertFailure("Selezionare una categoria da associare alla domanda");
        }
      } 
    } else if(sender == clear)
    {
      clear();
    }
  }
  
  /*
   * Metodo che ripristina le condizioni iniziali della sezione
   */
  private void clear()
  {
    questionText.setText("");
    categoryList.setSelectedIndex(0);
  }
  
}
