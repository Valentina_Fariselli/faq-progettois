package faq.client;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import faq.shared.Question;

/**
 * Classe che gestisce la sezione dell'interfaccia grafica per
 * la visualizzazione del testo di una domanda nella finestra
 * principale dell'applicazione
 */
public class QuestionButton extends Composite
{
  interface QuestionButtonBinder extends UiBinder<Widget, QuestionButton> {}
  private static QuestionButtonBinder uiBinder = GWT.create(QuestionButtonBinder.class);

  @UiField HTMLPanel questionContainer;
  @UiField Label questionText;
  
  private ClasseControllo controller;
  private Question question;
  
  public QuestionButton(ClasseControllo controller, Question q) 
  {
    this.controller = controller;
    this.question = q;
    
    initWidget(uiBinder.createAndBindUi(this));
    
    questionText.setText(q.getQuestion());
    questionContainer.setStyleName("questionContainer");
    attachHandler();
  }
  
  /*
   * Metodo per associare il clickHandler all'intero pannello della domanda
   * e non solamente alla Label che ne contiene il testo
   */
  private void attachHandler()
  {
    questionContainer.sinkEvents(Event.ONCLICK);
    questionContainer.addHandler(new ClickHandler()
    {
      @Override
      public void onClick(ClickEvent event)
      {
        Widget sender = (Widget) event.getSource();
        if(sender == questionContainer)
        {
          controller.extendedQuestionDialog(null, question.getQuestion(), null);
        } else
        {
          new NotificaAvviso("alert", "bottone");
        }
      }
    }, ClickEvent.getType());
  }
}
