package faq.shared;
/**
 * 
 */


import java.util.ArrayList;
import java.util.Calendar;

import java.util.GregorianCalendar;
import java.util.Iterator;

/**Classe che gestisce le risposte
 * @author Valentina Fariselli
 *
 */
public class Answer implements Collection, Comparable<Object>
{
	private String answer;
	private RegisteredUser user;
	private Calendar date = new GregorianCalendar();
	private ArrayList<Judgment> judgments = new ArrayList<>();
	
	// Empty constructor, for GWT compiler.
	@SuppressWarnings ( "unused")
	private Answer(){}
	
	public Answer(String answer, RegisteredUser user)
	{
		this.answer=answer;
		this.user=user;
		this.date.setTimeInMillis(System.currentTimeMillis());
	}
	
	public String getAnswer()
	{
		return answer;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public Calendar getDate()
	{
		return date;
	}
	
	public String getDay()
	{
	
		int day = date.get(Calendar.DAY_OF_MONTH);
		int month = (date.get(Calendar.MONTH) + 1);
		int year = date.get(Calendar.YEAR);
		return day + "/" + month + "/" + year;
	}
	
	public String getTime()
	{
		int hour = date.get(Calendar.HOUR_OF_DAY);
		int minute = date.get(Calendar.MINUTE);
		return hour + ":" + minute;
	}
	
	public void addJudgment(Judgment judgment)
	{
		judgments.add(judgment);
	}
	
	public Judgment getJudgment(int i)
	{
		return judgments.get(i);
	}
	
	public double getEverageJudgment()
	{
		int total = 0;
		for(Judgment j : judgments)
		{
			total = total + j.getJudgment();
		}
		if(judgments.size() == 0)
		{
			return 0;
		}
		else
		{
			return total/judgments.size();
		}
	}

	@Override
	public Iterator<Object> createIterator() {
		
		return new JudgmentIterator<>(judgments);
	}

	@Override
	public int compareTo(Object o) {
		
		if (this.judgments.size() == 0)
		{
			return getDate().compareTo(((Answer) o).getDate());
		}
		else
		{
			if(this.getEverageJudgment() == ((Answer) o).getEverageJudgment())
			{
				return getDate().compareTo(((Answer) o).getDate());
			}
			else if(this.getEverageJudgment() > ((Answer) o).getEverageJudgment())
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
		
	}

}
