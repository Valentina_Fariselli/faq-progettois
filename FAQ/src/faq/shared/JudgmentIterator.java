/**
 * 
 */
package faq.shared;

import java.util.ArrayList;
import java.util.Iterator;

/**Implementazione dell-interfaccia Iterator
 * Usata per scorrere i giudizi
 * @author Valentina Fariselli
 * @param <E>
 *
 */
public class JudgmentIterator<E> implements Iterator<E> {

	private ArrayList<Judgment> judgments;
	private int position;
	
	public JudgmentIterator(ArrayList<Judgment> judgments)
	{
		this.judgments = judgments;
	}
	
	@Override
	public boolean hasNext() 
	{
		if(judgments.size() <= position)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	@Override
	public E next() 
	{
		Judgment judgment = judgments.get(position);
		position++;
		return (E) judgment;
	}


}
