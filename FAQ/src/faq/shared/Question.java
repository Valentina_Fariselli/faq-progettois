package faq.shared;




import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

/**Classe per la gestione delle domande
 * 
 * @author Valentina Fariselli
 *
 */
public class Question extends CategoryComponent
{
	private String question;
	private RegisteredUser user;
	private Calendar date = new GregorianCalendar();
	private ArrayList<Answer> answers = new ArrayList<>();
	
	// Empty constructor, for GWT compiler.
	@SuppressWarnings ( "unused")
	private Question(){}
	
	public Question(String question, RegisteredUser user)
	{
		this.question=question;
		this.user=user;
		this.date.setTimeInMillis(System.currentTimeMillis());
	}
	
	public String getQuestion()
	{
		return question;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public Calendar getDate()
	{
		return date;
	}
	
	public String getDay()
	{
	
		int day = date.get(Calendar.DAY_OF_MONTH);
		int month = (date.get(Calendar.MONTH) + 1);
		int year = date.get(Calendar.YEAR);
		return day + "/" + month + "/" + year;
	}
	
	public String getTime()
	{
		int hour = date.get(Calendar.HOUR_OF_DAY);
		int minute = date.get(Calendar.MINUTE);
		return hour + ":" + minute;
	}
	
	public void addAnswer(Answer answer)
	{
		answers.add(answer);
	}
	
	public void removeAnswer(Answer answer)
	{
		answers.remove(answer);
	}
	
	public Answer getAnswer(int i)
	{
		return answers.get(i);
	}
	
	public Iterator<Object> createIterator()
	{
		return new AnswerIterator<>(answers);
	}
	
	@Override
	public int compareTo(Object o) {
		return getDate().compareTo(((Question) o).getDate());
	}
}
