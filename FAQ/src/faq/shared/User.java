package faq.shared;

public abstract class User
{
	protected String username;
	protected String password;
	protected String email;
	
	//Optional data
	protected String name;
	protected String surname;
	protected String sex;
	protected String birthDate;
	protected String birthPlace;
	protected String address;
	
	public User(){}
	
	
	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getSex() {
		return sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public String getAddress() {
		return address;
	}

	public String getUsername(){
		return username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getEmail(){
		return email;
	}
	

}
