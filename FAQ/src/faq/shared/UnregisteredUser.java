package faq.shared;


/**Implementazione utente non registrato
 * @author Valentina Fariselli
 *
 */
public class UnregisteredUser extends User 
{
	private static UnregisteredUser unregistered;
	
	private UnregisteredUser()
	{
	}
	
	public static UnregisteredUser get()
	{
		if(unregistered == null)
		{
			unregistered = new UnregisteredUser();
		}
		return unregistered;
	}
}
