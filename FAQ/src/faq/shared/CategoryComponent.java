package faq.shared;
import java.util.Calendar;
import java.util.Iterator;

/**
 * 
 */


/**Classe CategoryComponent
 * Implementa il pattern "composite"
 * @author Valentina Fariselli
 *
 */
public abstract class CategoryComponent implements Collection, Comparable<Object>
{
	public CategoryComponent()
	{}
	
	//metodi per la gestione delle categorie
	public void add(CategoryComponent component)
	{
		throw new UnsupportedOperationException();
	}
	
	public void rename(String name)
	{
		throw new UnsupportedOperationException();
	}
	
	public void remove(CategoryComponent component)
	{
		throw new UnsupportedOperationException();
	}
	
	public CategoryComponent getChild(int i)
	{
		throw new UnsupportedOperationException();
	}
	
	public String getName()
	{
		throw new UnsupportedOperationException();
	}
	
	//metodi per la gestione delle domande
	public String getQuestion()
	{
		throw new UnsupportedOperationException();
	}
	
	public User getUser()
	{
		throw new UnsupportedOperationException();
	}
	
	public Calendar getDate()
	{
		throw new UnsupportedOperationException();
	}
	
	public String getDay()
	{
		throw new UnsupportedOperationException();
	}
	
	public String getTime()
	{
		throw new UnsupportedOperationException();
	}
	
	public void addAnswer(Answer answer)
	{
		throw new UnsupportedOperationException();
	}
	
	public void removeAnswer(Answer answer)
	{
		throw new UnsupportedOperationException();
	}
	
	public Answer getAnswer(int i)
	{
		throw new UnsupportedOperationException();
	}
	
	public Iterator<?> createIterator()
	{
		throw new UnsupportedOperationException();
	}

	public int compareTo(Object o) {
		throw new UnsupportedOperationException();
	}

}
