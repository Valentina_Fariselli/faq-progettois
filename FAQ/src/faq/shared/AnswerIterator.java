/**
 * 
 */
package faq.shared;

import java.util.ArrayList;
import java.util.Iterator;

/**Implementazione dell-interfaccia Iterator
 * Usata per scorrere le risposte
 * @author Valentina Fariselli
 * @param <E>
 *
 */
public class AnswerIterator<E> implements Iterator<E> 
{
	private ArrayList<Answer> answers;
	private int position;
	
	public AnswerIterator(ArrayList<Answer> answers)
	{
		this.answers = answers;
	}
	
	@Override
	public boolean hasNext() 
	{
		if(answers.size() <= position)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	@Override
	public E next() 
	{
		Answer answer = answers.get(position);
		position++;
		return (E) answer;
	}

}
