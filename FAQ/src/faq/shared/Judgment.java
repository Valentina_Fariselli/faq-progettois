package faq.shared;
/**
 * 
 */


import java.util.Calendar;
import java.util.GregorianCalendar;

/**Classe che gestisce i giudizi
 * @author Valentina Fariselli
 *
 */
public class Judgment 
{
	private int judgment;
	private JudgeUser user;
	private Calendar date = new GregorianCalendar();
	
	// Empty constructor, for GWT compiler.
	@SuppressWarnings ( "unused")
	private Judgment(){}
	
	public Judgment(int judgment, JudgeUser user)
	{
		this.judgment=judgment;
		this.user=user;
		this.date.setTimeInMillis(System.currentTimeMillis());
	}
	
	public int getJudgment()
	{
		return judgment;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public Calendar getDate()
	{
		return date;
	}
	
	public String getDay()
	{
	
		int day = date.get(Calendar.DAY_OF_MONTH);
		int month = (date.get(Calendar.MONTH) + 1);
		int year = date.get(Calendar.YEAR);
		return day + "/" + month + "/" + year;
	}
	
	public String getTime()
	{
		int hour = date.get(Calendar.HOUR_OF_DAY);
		int minute = date.get(Calendar.MINUTE);
		return hour + ":" + minute;
	}
}
