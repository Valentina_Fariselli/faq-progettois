package faq.shared;


/**
 * 
 */

/**Implementazione utente registrato
 * @author Valentina Fariselli
 *
 */
public class NormalUser extends RegisteredUser 
{
	public NormalUser(String username, String password, String email)
	{
		this.username = username;
		this.password = password;
		this.email = email;
	}
	
	public NormalUser(String username, String password, String email, String name, String surname, String sex, String birthDate, String birthPlace, String address)
	{
		this.username = username;
		this.password = password;
		this.email = email;
		
		//Optional data
		this.name = name;
		this.surname = surname;
		this.sex = sex;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.address = address;
	}
}
