package faq.shared; 

/**Implementazione utente giudice
 * @author Valentina Fariselli
 *
 */
public class JudgeUser extends RegisteredUser 
{

	public JudgeUser(NormalUser user)
	{
		this.username = user.username;
		this.password = user.password;
		this.email = user.email;
		
		//Optional data
		this.name = user.name;
		this.surname = user.surname;
		this.sex = user.sex;
		this.birthDate = user.birthDate;
		this.birthPlace = user.birthPlace;
		this.address = user.address;
	}
}
