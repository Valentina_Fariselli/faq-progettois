package faq.shared;
/**
 * 
 */


import java.util.ArrayList;
import java.util.Iterator;

/**Classe di gestione delle categorie
 * @author Valentina Fariselli
 *
 */
public class Category extends CategoryComponent 
{
	private ArrayList<CategoryComponent> categoryComponents = new ArrayList<>();
	private String name;
	
	// Empty constructor, for GWT compiler.
	@SuppressWarnings ( "unused")
	private Category(){}
	
	public Category(String name)
	{
		this.name = name;
	}
	
	public void add(CategoryComponent component)
	{
		categoryComponents.add(component);
	}
	
	public void rename(String name)
	{
		this.name = name;
	}
	
	public void remove(CategoryComponent component)
	{
		categoryComponents.remove(component);
	}
	
	public CategoryComponent getChild(int i)
	{
		return categoryComponents.get(i);
	}
	
	public String getName()
	{
		return name;
	}
	
	public Iterator<CategoryComponent> createIterator()
	{
		return new CategoryIterator<>(categoryComponents);
	}
}
