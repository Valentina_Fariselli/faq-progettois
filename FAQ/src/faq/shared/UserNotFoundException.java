package faq.shared;
/*
 * Eccezione lanciata in caso di Utente non trovato nel db
 */
public class UserNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
