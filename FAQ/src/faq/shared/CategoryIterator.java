package faq.shared;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 */

/**Implementazione dell-interfaccia Iterator
 * Usata per scorrere categorie e domande
 * @author Valentina Fariselli
 * @param <E>
 *
 */
public class CategoryIterator<E> implements Iterator<E> {

	private ArrayList<CategoryComponent> components;
	private int position;
	
	public CategoryIterator(ArrayList<CategoryComponent> categoryComponents)
	{
		this.components = categoryComponents;
	}
	
	@Override
	public boolean hasNext() 
	{
		if( components.size() <= position)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public boolean hasNextCategory() 
	{
		if(components.get(position) instanceof Category)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
		
	@Override
	public E next() 
	{
		// TODO Auto-generated method stub
		CategoryComponent component = components.get(position);
		position++;
		return (E) component;
	}

}
