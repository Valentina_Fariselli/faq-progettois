package faq.shared;
/*
 * Eccezione lanciata in caso di Password sbagliata
 */
public class WrongPasswordException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
