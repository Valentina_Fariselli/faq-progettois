/**
 * 
 */
package faq.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;

import faq.shared.Answer;
import faq.shared.Category;
import faq.shared.Question;

/**Classe che gestisce la memorizzazione delle risposte
 *
 * Gestisce la memorizzazione di una nuova risposta all'interno dello stesso database 
 * che memorizza le domande e le categorie.
 * Permette di aggiungere o eliminare risposte relative a una determinata domanda.
 * @author Valentina Fariselli
 *
 */
public class AnswerHandlerImpl 
{
	// Database
	private DB db;
	// Lista radici alberi categorie
	private ConcurrentMap<String, Category> categoryMap;
	
	public AnswerHandlerImpl( DB db)
	{
		this.db = db;
		this.categoryMap = this.db.getTreeMap("category");
	}	

	public void addAnswer(Question question, Answer answer)
	{
		Category root = this.categoryMap.get("root");
		question.addAnswer(answer);
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
	
	public void removeAnswer(Question question, Answer answer)
	{
		Category root = this.categoryMap.get("root");
		question.removeAnswer(answer);
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
	
	// Tutte le risposte relative a una domanda
	public ArrayList<Answer> getAnswers(Question question)
	{
		Category root = this.categoryMap.get("root");
		ArrayList<Answer> res = new ArrayList<Answer>();
		Iterator sfogliaDomanda = question.createIterator();
		while(sfogliaDomanda.hasNext())
		{
			res.add((Answer) sfogliaDomanda.next());			
		}
		res.trimToSize();
		return res;
	}
}
