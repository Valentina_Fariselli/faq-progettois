package faq.server;

import java.io.File;
import java.util.ArrayList;


import org.mapdb.DB;
import org.mapdb.DBMaker;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import faq.client.FAQService;
import faq.shared.Answer;
import faq.shared.Category;
import faq.shared.DuplicatedUserNameException;
import faq.shared.Judgment;
import faq.shared.Question;
import faq.shared.User;
import faq.shared.UserNotFoundException;
import faq.shared.WrongPasswordException;
/*
 * Smista le chiamate ai 3 Services, per non far instanziare 3 server al client
 * 
 */
public class FAQServiceImpl extends RemoteServiceServlet implements FAQService {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		LoginServiceImpl loginService;
		AnswerHandlerImpl answerHandler;
		CategoryHandlerImpl categoryHandler;
		JudgmentHandlerImpl judgmentHandler;
		QuestionHandlerImpl questionHandler;
	

	public FAQServiceImpl() {
		DB db = DBMaker.newFileDB( new File( "FAQ-DB.txt" ) ).closeOnJvmShutdown().make();

		loginService = new LoginServiceImpl( db );
		answerHandler = new AnswerHandlerImpl( db);
		categoryHandler = new CategoryHandlerImpl( db);
		judgmentHandler = new JudgmentHandlerImpl( db );
		questionHandler = new  QuestionHandlerImpl( db, categoryHandler );
		
		// Per simulare.
		//restartDb(); 
		// Drop del db
		//populate();
	}

	// DEBUG svuotare il db
	public void restartDb() {
		loginService.clear();		
		loginService.addAdmin(); //Riaggiungo admin
		
		categoryHandler.addRoot();
		
	}

	// DEBUG popolare il db
	public void populate() {

		try {
			addUser( "Sergio" , "m" , "sergio@studio.it" , null , null , null , null , null , null );
			addUser( "Valentina" , "f" , "valentina@studio.it" , null , null , null , null , null , null );
		} catch ( DuplicatedUserNameException e ) {
			e.printStackTrace();
		}
	

	
	}

	public User addUser( String username, String password, String email, String name, String surname, String sex,
			String birthDate, String birthPlace, String address ) throws DuplicatedUserNameException {
		return loginService.addUser( username , password , email , name , surname , sex , birthDate , birthPlace ,
				address );
	}

	public User login( String userName, String password ) throws WrongPasswordException, UserNotFoundException {
		return loginService.login( userName , password );
	}
	
	public ArrayList<User> getRegistered() {
		return loginService.getRegistered();
	}
	
	public ArrayList<User> getAllUsers() {
		return loginService.getAllUsers();
	}
	
	public ArrayList<User> nominateJudge( String userName ){
		return loginService.nominateJudge( userName );
	}
	public ArrayList<User> getJudges() {
		return loginService.getJudges();
	}

	
	public ArrayList<Question> addQuestion(Category category, Question question) {
		questionHandler.addQuestion(category, question);
		return getQuestions(category);
	}
	
	@Override
	public ArrayList<Question> getQuestions(Category category) {
		return getQuestions(category);
	}
	
	@Override
	public ArrayList<Question> removeQuestion(Category category, Question question, User user) {
		questionHandler.removeQuestion(category, question);
		return getQuestions(category);
	}
	
	@Override
	public Question addAnswer(Answer answer, Question question) {
		answerHandler.addAnswer(question, answer);
		return question;
	}
	
	
	public ArrayList<Answer> getAnswers(Question question) {
		return answerHandler.getAnswers(question);
	}

	@Override
	public Question removeAnswer(Question question, User user, Answer answer) {
		answerHandler.removeAnswer(question, answer);
		return question;
	}
	
	@Override
	public Category renameCategory(Category category, String name) {
		categoryHandler.renameCategory(category, name);
		return getAllCategory();
	}

	@Override
	public Category addCategory(String categoryName) {
		categoryHandler.addCategory(categoryName);
		return getAllCategory();
	}

	@Override
	public Category addSubCategory(Category category, String name) {
		categoryHandler.addSubCategory(category, name);
		return getAllCategory();
	}

	@Override
	public Question getQuestions(Category category, String question) {
		return getQuestions(category, question);
	}

	@Override
	public Answer addJudgment(Answer answer, Judgment judgment) {
		judgmentHandler.addJudgment(answer, judgment);
		return answer ;
	}

	@Override
	public Category getAllCategory() {
		return categoryHandler.getAllCategory();
	}

	@Override
	public ArrayList<Question> getAllQuestions() {
		return questionHandler.getAllQuestions();
	}

	
	
}
