/**
 * 
 */
package faq.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;

import faq.shared.Answer;
import faq.shared.Category;
import faq.shared.Judgment;
import faq.shared.Question;

/**Classe che gestisce la memorizzazione dei giudizi
 *
 * Gestisce la memorizzazione di un nuovo giudizio all'interno dello stesso database 
 * che memorizza le risposte, le domande e le categorie.
 * Permette di aggiungere giudizi relative a una determinata risposta.
 * @author Valentina Fariselli
 *
 */
public class JudgmentHandlerImpl 
{
	// Database
	private DB db;
	// Lista radici alberi categorie
	private ConcurrentMap<String, Category> categoryMap;
	
	public JudgmentHandlerImpl( DB db)
	{
		this.db = db;
		this.categoryMap = this.db.getTreeMap("category");
	}	

	public void addJudgment(Answer answer, Judgment judgment)
	{
		Category root = this.categoryMap.get("root");
		answer.addJudgment(judgment);;
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
	
	// Tutte i giudizi relative a una risposta
	public ArrayList<Judgment> getJudgments(Answer answer)
	{
		Category root = this.categoryMap.get("root");
		ArrayList<Judgment> res = new ArrayList<Judgment>();
		Iterator sfogliaRisposta = answer.createIterator();
		while(sfogliaRisposta.hasNext())
		{
			res.add((Judgment) sfogliaRisposta.next());			
		}
		res.trimToSize();
		return res;
	}
}
