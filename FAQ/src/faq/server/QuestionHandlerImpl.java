
package faq.server;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;

import faq.shared.Category;
import faq.shared.CategoryComponent;
import faq.shared.CategoryIterator;

import faq.shared.Question;

/**Classe che gestisce la memorizzazione delle domande
 *
 * Gestisce la memorizzazione di una nuova domanda all'interno dello stesso database 
 * che memorizza le categorie.
 * Permette di aggiungere o eliminare domande relative a una determinata categoria.
 * @author Valentina Fariselli
 *
 */
public class QuestionHandlerImpl 
{
	// Database
	private DB db;
	// Lista radici alberi categorie
	private ConcurrentMap<String, Category> categoryMap;
	// oggetto per la gestione delle categorie
	private CategoryHandlerImpl categoryHandler;
	
	public QuestionHandlerImpl( DB db, CategoryHandlerImpl categoryHandler)
	{
		this.db = db;
		this.categoryMap = this.db.getTreeMap("category");
		this.categoryHandler = categoryHandler;
		
	}
	
	public void addQuestion(Category category, Question question)
	{
		Category root = this.categoryMap.get("root");
		Category categoria = categoryHandler.getCategory(category.getName(), root);
		categoria.add(question);
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
	
	public void removeQuestion(Category category, Question question)
	{
		Category root = this.categoryMap.get("root");
		Category categoria = categoryHandler.getCategory(category.getName(), root);
		categoria.remove(question);
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
	
	
	// Tutte le domande relative a una categoria
	public ArrayList<Question> getQuestions(Category category)
	{
		Category root = this.categoryMap.get("root");
		Category categoria = categoryHandler.getCategory(category.getName(), root);
		ArrayList<Question> res = new ArrayList<Question>();
		CategoryIterator sfogliaCategoria = (CategoryIterator) categoria.createIterator();
		while(sfogliaCategoria.hasNext())
		{
			CategoryComponent componente = (CategoryComponent) sfogliaCategoria.next();
			if( componente instanceof Question )
			{
				res.add( (Question)componente );
			}
			else
			{
				res.addAll(getQuestions((Category) componente));
			}
		}
		res.trimToSize();
		return res;
	}
	
	public ArrayList<Question> getAllQuestions()
	{
		Category root = this.categoryMap.get("root");
		return getQuestions(root);
	}
}
