package faq.server;
import faq.shared.*;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentMap;
import org.mapdb.DB;



/*
 * Gestisce le operazioni sugli users
 */
public class LoginServiceImpl {
	
	// Database
	private DB db;
	// Lista utenti
	private ConcurrentMap<String, User> userMap;
	
	public LoginServiceImpl( DB db){
		this.db = db;
		this.userMap = this.db.getTreeMap("user");
		
		if( userMap.get("admin") == null){ // l'admin deve essere sempre presente nel db
			addAdmin();
		}
	}
	
	// Agigunge un utente al db, se gia'† presente lancia eccezione 
	public User addUser( String username, String password, String email, String name, String surname, String sex, String birthDate, String birthPlace, String address ) throws DuplicatedUserNameException {
		if( userMap.get( username ) != null ){
			throw new DuplicatedUserNameException( username );
		}
		else{
			User user = new NormalUser(username,password,email,name,surname,sex,birthDate,birthPlace,address);
			
			this.userMap.put( username, user );
			this.db.commit();
			return user;
		}
	}
	
	// Metodo che azzera la collezione utenti
	public void clear(){
		userMap.clear();
	}
	
	public void remove( String userName){
		userMap.remove( userName );
	}
	
	
	// Aggiunge l'utente admin
	public void addAdmin(){
		User admin = new AdminUser("admin", "admin", "admin@admin.com", null, null, null, null, null, null);
		userMap.put("admin", admin);
		this.db.commit();
	}

	// Controlla username e password, se l'utente non e' presente o la password e' sbagliata lancia eccezioni
	public User login(String userName, String password) throws WrongPasswordException, UserNotFoundException {
		User user = this.userMap.get( userName );
		if( user != null ){
			if( !user.getPassword().equals(password) ){
				throw new WrongPasswordException();
			}
		}
		else{
			throw new UserNotFoundException();
		}
		return user;
	}
	
	// Tutti gli utenti ( funzionalit√† admin )
		public ArrayList<User> getAllUsers(){
			ArrayList<User> res = new ArrayList<User>(userMap.values());
			return res;
		}
		
		// Tutti gli utenti non giudici o admin ( funzionalit√† admin )
		public ArrayList<User> getRegistered(){
			ArrayList<User> users = new ArrayList<User>( userMap.values() );
			ArrayList<User> res = new ArrayList<User>();
			for( User u : users ){
				if( u instanceof NormalUser ){
					res.add( u );
				}
			}
			res.trimToSize();
			return res;
		}
		
		// Nomina un giudice sostituendo l'istanza di utente normale 
		// con quella di utente giudice, associandola allo stesso nome
		public ArrayList<User> nominateJudge( String userName ){
			NormalUser normalToJudge = (NormalUser) userMap.get(userName);
			//judge.setRoleJudge();
			User judge = new JudgeUser(normalToJudge);
			userMap.replace(userName, normalToJudge, judge);
			this.db.commit();
			return getRegistered();
		}

		
		// Tutti gli utenti giudici ( funzionalit√† admin )
		public ArrayList<User> getJudges(){
			ArrayList<User> users = new ArrayList<User>( userMap.values() );
			ArrayList<User> res = new ArrayList<User>();
			for( User u : users){
				if( u instanceof JudgeUser ){
					res.add( u );
				}
			}
			res.trimToSize();
			return res;
		}
}