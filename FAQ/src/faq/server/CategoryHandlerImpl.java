/**
 * 
 */
package faq.server;

import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;

import faq.shared.Category;
import faq.shared.CategoryComponent;
import faq.shared.CategoryIterator;


/**Classe che gestisce la memorizzazione delle categorie
 * Gestisce la memorizzazione di una nuova categoria principale,
 * di una nuova sottocategoria o di rinominare una categoria
 * @author Valentina Fariselli
 *
 */
public class CategoryHandlerImpl 
{
	
	// Database
	private DB db;
	// Lista radici alberi categorie
	private ConcurrentMap<String, Category> categoryMap;
	
	public CategoryHandlerImpl( DB db)
	{
		this.db = db;
		this.categoryMap = this.db.getTreeMap("category");
		
		if( categoryMap.get("root") == null)
		{
			addRoot();
		}
	}
	
	public void addRoot()
	{
		//categoria di controllo: non contiene domande o altre categorie
		Category allCategories = new Category("root");
		Category ambiente = new Category("Ambiente");
		Category animali = new Category("Animali");
		Category arte = new Category("Arte e cultura");
		Category elettronica = new Category("Elettronica e tecnologia");
		Category sport = new Category("Sport");
		Category svago = new Category("Svago");
		
		allCategories.add(ambiente);
		allCategories.add(animali);
		allCategories.add(arte);
		allCategories.add(elettronica);
		allCategories.add(sport);
		allCategories.add(svago);
		
		categoryMap.put("root", allCategories);
		/*
		categoryMap.put("Ambiente", ambiente);
		categoryMap.put("Animali", animali);
		categoryMap.put("Arte e cultura", arte);
		categoryMap.put("Elettronica e tecnologia", elettronica);
		categoryMap.put("Sport", sport);
		categoryMap.put("Svago", svago);
		*/
		this.db.commit();
	}
	
	public Category getAllCategory(){
		
		return this.categoryMap.get("root");
	}
	
	public void addSubCategory(Category category,String subCategoryName)
	{
		
		Category root = this.categoryMap.get("root");
		Category categoria = getCategory(category.getName(), root);
		categoria.add(new Category(subCategoryName));
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
	
	public Category getCategory(String categoryName, Category current)
	{
		if(current.getName().equals(categoryName))
		{
			return current;
		}
		else 
		{
			CategoryIterator sfogliaCategoria = (CategoryIterator) current.createIterator();
			while(sfogliaCategoria.hasNext())
			{
				CategoryComponent componente = (CategoryComponent) sfogliaCategoria.next();
				if (componente instanceof Category)
				{
					Category categoria = (Category) componente;
					Category res = getCategory(categoryName, categoria);
					if(res != null)
					{
						return res;
					}
					
				}
				
			}
		}
		return null;
	}
	
	public void addCategory(String categoryName)
	{
		Category root = this.categoryMap.get("root");
		root.add(new Category(categoryName));
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
	
	public void renameCategory(Category category,String name)
	{
		Category root = this.categoryMap.get("root");
		Category categoria = getCategory(category.getName(), root);
		categoria.rename(name);
		categoryMap.replace("root", categoryMap.get("root"), root);
		this.db.commit();
	}
}
