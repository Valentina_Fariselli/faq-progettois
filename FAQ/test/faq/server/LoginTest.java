package faq.server;
import java.io.File;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import faq.shared.DuplicatedUserNameException;
import faq.shared.User;
import faq.shared.UserNotFoundException;
import faq.shared.WrongPasswordException;
import junit.framework.TestCase;


public class LoginTest extends TestCase {
	DB db = DBMaker.newFileDB( new File( "FaqTest1.txt" ) ).closeOnJvmShutdown().make();
	LoginServiceImpl service = new LoginServiceImpl( db );
	
	public void setUp(){
		service.clear();
	}
	
	public void testLogin()throws WrongPasswordException, UserNotFoundException {
		String userName = "Tiger992";
		String password = "Sergio1992";
		String email = "s.strappy@gmail.com";
		
		try {
			service.addUser( userName , password , email , null , null , null , null , null , null );
		} catch ( DuplicatedUserNameException e ) {
			e.printStackTrace();
		}
		
		User user = service.login( userName, password);
		
		assertTrue( user.getUsername().equals(userName));
		assertTrue( user.getPassword().equals(password));
		assertTrue( user.getEmail().equals( email ));	
	}
	
	public void testLoginComplete()throws WrongPasswordException, UserNotFoundException {
		String userName = "Valentina";
		String password = "vale93";
		String email = "valentina@gmail.com";
		String name = "Valentina";
		String surname = "Rossi";
		String sex = "female";
		String birthDate = "15/11/1993";
		String birthPlace = "Bologna";
		String address = "Via Sanzio 14";
		
		try {
			service.addUser( userName , password , email , name , surname , sex , birthDate , birthPlace , address );
		} catch ( DuplicatedUserNameException e ) {
			e.printStackTrace();
		}
		
		User user = service.login( userName, password);
		assertEquals( user.getUsername(), userName);
		assertEquals( user.getPassword(), password);
		assertEquals( user.getEmail(), email);
		assertEquals( user.getName(), name);
		assertEquals( user.getSurname(), surname);
		assertEquals( user.getSex(), sex);
		assertEquals( user.getBirthDate(), birthDate);
		assertEquals( user.getBirthPlace(), birthPlace);
		assertEquals( user.getAddress(), address);
	}
}
