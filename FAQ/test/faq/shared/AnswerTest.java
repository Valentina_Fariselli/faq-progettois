package faq.shared;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Valentina Fariselli
 *
 */

public class AnswerTest {

	@Test
	public final void testAnswer() {
		Answer answer = new Answer("test answer", new NormalUser("testUser", "password", "email@email.com"));
		
		assertTrue(answer.getAnswer().equals("test answer"));
	}

	@Test
	public final void testGetAnswer() {
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		Answer answer1 = new Answer("test answer", normalUser);
		Answer answer2 = new Answer("test answer 2", normalUser);
		
		assertTrue(answer1.getAnswer().equals("test answer"));
		assertTrue(answer2.getAnswer().equals("test answer 2"));
	}

	@Test
	public final void testGetUser() {
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		RegisteredUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Answer answer1 = new Answer("test answer", normalUser);
		Answer answer2 = new Answer("test answer 2", judgeUser);
		
		assertTrue(answer1.getUser().equals(normalUser));
		assertTrue(answer2.getUser().equals(judgeUser));
	}

	@Test
	public final void testGetDate() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testAddJudgment() {
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		RegisteredUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Answer answer = new Answer("test answer", normalUser);
		answer.addJudgment(new Judgment(5, (JudgeUser) judgeUser));
		
		assertTrue(answer.getJudgment(0).getJudgment() == 5);
	}

	@Test
	public final void testGetJudgment() {
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		RegisteredUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Answer answer = new Answer("test answer", normalUser);
		answer.addJudgment(new Judgment(5, (JudgeUser) judgeUser));
		answer.addJudgment(new Judgment(3, (JudgeUser) judgeUser));
		answer.addJudgment(new Judgment(1, (JudgeUser) judgeUser));
		
		assertTrue(answer.getJudgment(1).getJudgment() == 3);
	}

	@Test
	public final void testGetEverageJudgment() {
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		RegisteredUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Answer answer = new Answer("test answer", normalUser);
		answer.addJudgment(new Judgment(5, (JudgeUser) judgeUser));
		answer.addJudgment(new Judgment(3, (JudgeUser) judgeUser));
		answer.addJudgment(new Judgment(1, (JudgeUser) judgeUser));
		
		assertTrue(answer.getEverageJudgment() == 3);
	}

	@Test
	public final void testCreateIterator() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testCompareTo() {
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		RegisteredUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Answer answer1 = new Answer("test answer 1", normalUser);
		answer1.addJudgment(new Judgment(5, (JudgeUser) judgeUser));
		answer1.addJudgment(new Judgment(3, (JudgeUser) judgeUser));
		answer1.addJudgment(new Judgment(1, (JudgeUser) judgeUser));
		Answer answer2 = new Answer("test answer 2", normalUser);
		answer2.addJudgment(new Judgment(5, (JudgeUser) judgeUser));
		answer2.addJudgment(new Judgment(3, (JudgeUser) judgeUser));
		answer2.addJudgment(new Judgment(1, (JudgeUser) judgeUser));
		Answer answer3 = new Answer("test answer", normalUser);
		
		assertTrue(answer1.compareTo(answer2) == 0);
		assertTrue(answer1.compareTo(answer3) == 1);
	}

}
