package faq.shared;

import static org.junit.Assert.*;

import org.junit.Test;



/**
 * @author Valentina Fariselli
 *
 */
public class UserTest {

	@Test
	public final void testUser() {
		User unregistered = UnregisteredUser.get();
		User registered = new NormalUser("pippo", "password", "email");
		User judge = new JudgeUser((NormalUser) registered);
		User admin = new AdminUser("pluto", "password", "email");
		
		assertTrue(unregistered instanceof UnregisteredUser);
		assertTrue(registered instanceof RegisteredUser);
		assertTrue(registered instanceof NormalUser);
		assertTrue(judge instanceof RegisteredUser);
		assertTrue(judge instanceof JudgeUser);
		assertTrue(admin instanceof AdminUser);
	}


}
