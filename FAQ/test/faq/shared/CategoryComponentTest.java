/**
 * 
 */
package faq.shared;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.junit.Test;

/**
 * @author Valentina Fariselli
 *
 */
public class CategoryComponentTest {

	/**
	 * Test method for {@link faq.shared.CategoryComponent#CategoryComponent()}.
	 */
	@Test
	public final void testCategoryComponent() {
		CategoryComponent category = new Category("root");
		Question question = new Question("test question", new NormalUser("testUser", "password", "email@email.com"));
		
		assertTrue(question instanceof Question);
		assertTrue(category instanceof Category);
		assertTrue(category instanceof CategoryComponent && question instanceof CategoryComponent);
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#add(faq.shared.CategoryComponent)}.
	 */
	@Test
	public final void testAdd() {
		Category category = new Category("root");
		category.add(new Question("test question", new NormalUser("testUser", "password", "email@email.com")));
		category.add(new Category("test"));
		
		assertTrue(category.getChild(0) instanceof Question);
		assertTrue(category.getChild(1) instanceof Category);
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#rename(java.lang.String)}.
	 */
	@Test
	public final void testRename() {
		Category category = new Category("test");
		category.rename("newName");
		
		assertFalse(category.getName().equals("test"));
		assertTrue(category.getName().equals("newName"));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#remove(faq.shared.CategoryComponent)}.
	 */
	@Test
	public final void testRemove() {
		Category category = new Category("root");
		Question question = new Question("test question", new NormalUser("testUser", "password", "email@email.com"));
		category.add(question);
		category.add(new Category("test"));
		category.remove(question);
		
		assertFalse(category.getChild(0) instanceof Question);
		assertTrue(category.getChild(0) instanceof Category);
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getChild(int)}.
	 */
	@Test
	public final void testGetChild() {
		Category category = new Category("root");
		category.add(new Question("test question", new NormalUser("testUser", "password", "email@email.com")));
		category.add(new Category("test"));
		CategoryComponent question = category.getChild(0);
		CategoryComponent subCategory = category.getChild(1);
		
		assertTrue(question instanceof Question);
		assertTrue(subCategory instanceof Category);
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getName()}.
	 */
	@Test
	public final void testGetName() throws UnsupportedOperationException{
		Category category = new Category("Prova nome Complesso 123");
		
		assertTrue( category.getName().equals("Prova nome Complesso 123"));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getQuestion()}.
	 */
	@Test
	public final void testGetQuestion() throws UnsupportedOperationException{
		Question question = new Question("test question", new NormalUser("testUser", "password", "email@email.com"));
		
		assertTrue(question.getQuestion().equals("test question"));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getUser()}.
	 */
	@Test
	public final void testGetUser() throws UnsupportedOperationException{
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		Question question1 = new Question("test question 1", normalUser);
		RegisteredUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Question question2 = new Question("test question 2", judgeUser);
		
		assertTrue(question1.getUser().equals(normalUser));
		assertTrue(question2.getUser().equals(judgeUser));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getDate()}.
	 */
	@Test
	public final void testGetDate() throws UnsupportedOperationException{
		Question question = new Question("test question", new NormalUser("testUser", "password", "email@email.com"));
		Calendar date = new GregorianCalendar();
		date.setTimeInMillis(System.currentTimeMillis());
		
		assertTrue(question.getDate().equals(date));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getDay()}.
	 */
	@Test
	public final void testGetDay() throws UnsupportedOperationException{
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getTime()}.
	 */
	@Test
	public final void testGetTime() throws UnsupportedOperationException{
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#addAnswer(faq.shared.Answer)}.
	 */
	@Test
	public final void testAddAnswer() throws UnsupportedOperationException {
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		Question question = new Question("test question 1", normalUser);
		question.addAnswer(new Answer("test answer", normalUser));
		
		assertTrue(question.getAnswer(0).getAnswer().equals("test answer"));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#removeAnswer(faq.shared.Answer)}.
	 */
	@Test
	public final void testRemoveAnswer() throws UnsupportedOperationException{
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		Question question = new Question("test question 1", normalUser);
		Answer answer = new Answer("test answer 1", normalUser);
		question.addAnswer(answer);
		question.addAnswer(new Answer("test answer 2", normalUser));
		question.removeAnswer(answer);
		
		assertFalse(question.getAnswer(0).getAnswer().equals("test answer 1"));
		assertTrue(question.getAnswer(0).getAnswer().equals("test answer 2"));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#getAnswer(int)}.
	 */
	@Test
	public final void testGetAnswer() throws UnsupportedOperationException{
		RegisteredUser normalUser = new NormalUser("testUser", "password", "email@email.com");
		Question question = new Question("test question 1", normalUser);
		question.addAnswer(new Answer("test answer", normalUser));
		question.addAnswer(new Answer("test answer 2", normalUser));
		
		assertTrue(question.getAnswer(0).getAnswer().equals("test answer"));
		assertTrue(question.getAnswer(1).getAnswer().equals("test answer 2"));
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#createIterator()}.
	 */
	@Test
	public final void testCreateIterator() {
		Category category = new Category("root");
		Question question = new Question("test question", new NormalUser("testUser", "password", "email@email.com"));
		category.add(question);
		category.add(new Category("test"));
		Iterator sfogliaCategoria = category.createIterator();
		while(sfogliaCategoria.hasNext())
		{
			CategoryComponent componente = (CategoryComponent) sfogliaCategoria.next();
			if(componente instanceof Category)
			{
				assertTrue(componente.getName().equals("test"));
			}
			else
			{
				assertTrue(componente.getQuestion().equals("test question"));
			}
		}
	}

	/**
	 * Test method for {@link faq.shared.CategoryComponent#compareTo(java.lang.Object)}.
	 */
	@Test
	public final void testCompareTo() {
		fail("Not yet implemented"); // TODO
	}

}
