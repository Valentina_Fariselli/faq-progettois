package faq.shared;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Valentina Fariselli
 *
 */
public class JudgmentTest {

	@Test
	public final void testJudgment() {
		Judgment judgment = new Judgment(5, new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com")));
		
		assertTrue(judgment.getJudgment() == 5);
	}

	@Test
	public final void testGetJudgment() {
		JudgeUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Judgment judgment1 = new Judgment(5, judgeUser);
		Judgment judgment2 = new Judgment(1, judgeUser);
		
		assertTrue(judgment1.getJudgment() == 5);
		assertTrue(judgment2.getJudgment() == 1);
	}

	@Test
	public final void testGetUser() {
		JudgeUser judgeUser = new JudgeUser(new NormalUser("testJudgeUser", "password", "email@email.com"));
		Judgment judgment = new Judgment(5, judgeUser);
		
		assertTrue(judgment.getUser().equals(judgeUser));
	}

}
